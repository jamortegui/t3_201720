package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;


import com.sun.org.apache.bcel.internal.generic.LUSHR;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.Iterator;
import model.data_structures.Queue;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{


	private DoubleLinkedList<BusUpdateVO> listaBuses;
	private Queue<BusUpdateVO> buses;
	private  DoubleLinkedList<StopVO> stops;
	
	public STSManager()
	{
		listaBuses = new DoubleLinkedList<BusUpdateVO>();
		buses = new Queue<BusUpdateVO>();
	}
	
	@Override
	public void readBusUpdate(File rtFile) {

		int i =0;

		// TODO Auto-generated method stub

		JsonParser jsonParser = new JsonParser();


		try
		{

			if(listaBuses.getSize()==0){
				System.out.println("Entra");
				JsonElement jsonElement = jsonParser.parse(new FileReader(rtFile));
				JsonArray jsonArray = jsonElement.getAsJsonArray();

				String nVehiculo;
				String tRegistrado;
				String linkMapaRuta;
				Integer idTrip;
				String nRuta;
				String direccion;
				String destino;
				String patron;
				Double latitud;
				Double longitud;


				JsonPrimitive vehicleNo;
				JsonPrimitive tripId;
				JsonPrimitive routeNo;
				JsonPrimitive direction;
				JsonPrimitive destination;
				JsonPrimitive pattern;
				JsonPrimitive latitude;
				JsonPrimitive longitude;
				JsonPrimitive recordedTime;
				JsonObject routeMap;
				JsonPrimitive linkRouteMap;

				JsonObject jObjectActual;
				for(JsonElement jElementActual : jsonArray)
				{
					jObjectActual = jElementActual.getAsJsonObject();					
					vehicleNo = jObjectActual.getAsJsonPrimitive("VehicleNo");
					nVehiculo = vehicleNo.getAsString();					
					tripId = jObjectActual.getAsJsonPrimitive("TripId");
					idTrip = new Integer( tripId.getAsInt() );

					routeNo = jObjectActual.getAsJsonPrimitive("RouteNo");
					nRuta = routeNo.getAsString();

					direction = jObjectActual.getAsJsonPrimitive("Direction");
					direccion = direction.getAsString();

					destination = jObjectActual.getAsJsonPrimitive("Destination");
					destino = destination.getAsString();

					pattern = jObjectActual.getAsJsonPrimitive("Pattern");
					patron = pattern.getAsString();

					latitude = jObjectActual.getAsJsonPrimitive("Latitude");
					latitud = latitude.getAsDouble();

					longitude = jObjectActual.getAsJsonPrimitive("Longitude");
					longitud = longitude.getAsDouble();

					recordedTime = jObjectActual.getAsJsonPrimitive("RecordedTime");
					tRegistrado = recordedTime.getAsString();

					routeMap = jObjectActual.getAsJsonObject("RouteMap");
					linkRouteMap = routeMap.getAsJsonPrimitive("Href");
					linkMapaRuta = linkRouteMap.getAsString();


					BusUpdateVO bus = new BusUpdateVO(nVehiculo, idTrip, nRuta, direccion, destino, patron, latitud, longitud, tRegistrado, linkMapaRuta);
					listaBuses.addAtEnd(bus);
				
					Double[] pos = new Double[2];
					pos[0]=latitud;
					pos[1]=longitud;
					
					bus.agregarPosicion(pos);
				}
			}

			else{
				JsonElement jsonElement = jsonParser.parse(new FileReader(rtFile));
				JsonArray jsonArray = jsonElement.getAsJsonArray();
				String nVehiculo;				
				Double latitud;
				Double longitud;
				JsonPrimitive vehicleNo;				
				JsonPrimitive latitude;
				JsonPrimitive longitude;				
			
				JsonObject jObjectActual;
				for(JsonElement jElementActual : jsonArray)
				{
					jObjectActual = jElementActual.getAsJsonObject();					
					vehicleNo = jObjectActual.getAsJsonPrimitive("VehicleNo");
					nVehiculo = vehicleNo.getAsString();				
					latitude = jObjectActual.getAsJsonPrimitive("Latitude");
					latitud = latitude.getAsDouble();
					longitude = jObjectActual.getAsJsonPrimitive("Longitude");
					longitud = longitude.getAsDouble();					
					BusUpdateVO buscada = buscada(nVehiculo);
					
					System.err.println(listaBuses.getElement(144));
					Double[] pos = new Double[2];
					pos[0]=latitud;
					pos[1]=longitud;
					
					buscada.agregarPosicion(pos);
					
				}



			}
		}

		catch(Exception e)
		{
			System.out.println("Ha ocurrido un problema.  " +e.getCause()
					+ " : " + e.getClass());
			
			
		}



	}



	@Override
	public IStack<StopVO> listStops(Integer tripID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void loadStops() {
			try{
				stops= new DoubleLinkedList<StopVO>();
				FileReader fr=new FileReader("data/csv/stop.txt");
				BufferedReader br = new BufferedReader(fr);	String lin=br.readLine();
				lin=br.readLine();
				while(lin != null){			
					String[] ln = lin.split(",");				
					System.err.println(Arrays.toString(ln));
					StopVO nueva = new StopVO(Integer.parseInt(ln[0]), ln[1].trim(), ln[2].trim(), ln[3].trim(), Double.parseDouble(ln[4]), Double.parseDouble(ln[5]), ln[6].trim(), ln[7].trim(), ln[8]); 
					stops.addAtEnd(nueva);	
					lin=br.readLine();
				}
				br.close();
				fr.close();
				System.out.println("Tama�o " +stops.getSize());

			}		
			catch (IOException e) {
				e.printStackTrace();
			}			
		
		// TODO Auto-generated method stub

	}

	private BusUpdateVO buscada(String arg0){
		BusUpdateVO buscada=null;
		model.data_structures.Iterator<BusUpdateVO> iter =(Iterator<BusUpdateVO>) listaBuses.iterator();			
		while(iter.hasNext()){		
			BusUpdateVO actual = iter.next();
			if(actual.getnVehiculo().equals(arg0)){
				buscada=actual;
				break;
			}									
		}
		return buscada;
	}
}
