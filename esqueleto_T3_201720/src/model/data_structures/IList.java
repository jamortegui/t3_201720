package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {
	int getSize();
	void add (T t);
	void addAtEnd(T t);
	void addAtK(int k, T t);
	T getElement(int k);
	T getCurrentElement();
	void delete(T t);
	void deleteAtK(int k);	
	T getNext();
	T getPrevious();
}
