package model.data_structures;
public class Iterator<T> implements java.util.Iterator<T> {
	int lastAccess;
	T[] array;
	public Iterator(T[] array) {
		this.array=array;
		lastAccess=-1;
	}
	public boolean hasNext() {
		return ((lastAccess==(array.length-1))||(array.length==0))?false:true;
	}
	public T next() {
		try
		{
		T resultado= array[lastAccess+1];
		lastAccess++;
		return resultado;
		
		}
		
		
		catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}		
	}
	
	public T[] getLast(){
		return array;
	}
	public int getLastt(){
		return lastAccess;
	}
	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
}