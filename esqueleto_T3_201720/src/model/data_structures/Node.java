package model.data_structures;

public class Node <T>{	
	private T value ;	

	private Node<T> previous;
	private Node<T> next;

	public Node(Node<T> pre, Node<T> post, T value) {
		super();
		this.previous = pre;
		this.next = post;
		this.value = value;
	}
	public Node<T> getNext() {
		return next;
	}
	public void setPrevious(Node<T> pre) {
		this.previous = pre;
	}
	public Node<T> getPrevious() {
		return previous;
	}
	public void setNext(Node<T> post) {
		this.next = post;
	}	
	public boolean hasNext(){
		return (next==null)?false:true;
	}
	public boolean hasPrevious(){
		return (previous==null)?false:true;
	}
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "value=" + value + ", previous=" + previous + ", next=" + next ;
	}
	

}
