package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RingList<T> implements IList<T> {	
	private int n;
	private Node<T> head;
	private Node<T> last;
	private Node<T> current;
	public RingList() {		
		this.n = 0;	
		head=null;
		last=null;
		current=null;
	}
	public int getSize() {return n;}
	public T getNext() throws NoSuchElementException{
		if(isEmpty()&&n==0){
			throw new NoSuchElementException();
		}
		if(current==null&&n!=0){
			current= head;
			return head.getValue();
		}
		else{
			current=current.getNext();
			return current.getValue();
		}		
	}
	public T getPrevious() {
		if(current==null&&n==0){
			throw new NoSuchElementException();			
		}
		else if(current==null&&n>0)
		{
			current=head;
		}
		current=current.getPrevious();
		return current.getValue();
	}

	//	}		

	public boolean isEmpty() {
		return (head==null)?true:false;
	}
	public void addAtEnd(T t) {
		if(isEmpty()){
			head=new Node<T>(null, null,t);
			last= head;
		}	
		else{
			Node<T> nuevo = new Node<T>(last, head, t);
			last.setNext(nuevo);
			last=nuevo;
		}
		n++;
	}	
	public void addAtK(int k, T t) throws IndexOutOfBoundsException{
		
		if(k==0&&(head!=null)){
			if(n==1)
			{
				last=head;
				head=new Node<T>(last, last, t);
				last.setNext(head);
				last.setPrevious(head);
			}
			else 
			{
				Node<T> nuevo = new Node<T>(last, head, t);
				head.setPrevious(nuevo);
				last.setNext(nuevo);
				head=nuevo;
			}
		}
		else if(k==0&&(head==null)){
			head = new Node<T>(null, null, t);
		}
		else if(k==(n)){
			Node<T> nuevo = new Node<T>(last, null, t);
			last.setNext(nuevo);
			last=nuevo;
		}
		else {
			int l =0;
			current=head;
			while(l<(k%n)){
				current=current.getNext();
				l++;
			}			
			Node<T> nuevo = new Node<T>(current.getPrevious(), current, t);
			current.getPrevious().setNext(nuevo);
			current.setPrevious(nuevo);
			current=head;
		}
		n++;
	}	
	public T getCurrentElement() {
		if(current==null&&n==0){
			throw new NoSuchElementException();
		}
		else if(current==null&&n!=0){
			current=head;
			return current.getValue();
		}
		else {
		return current.getValue();
		}
	}	
	public void deleteAtK(int k) throws NoSuchElementException{
		if(k>n){
			throw new NoSuchElementException();
		}
		if(k==0&&head==null){
			throw new NoSuchElementException();
		}
		else if (k==0&&head!=null){
			head=head.getNext();
			head.setPrevious(last);
			n--;
		}
		else if(k==(n-1)){
			last= last.getPrevious();
			last.setNext(head);
			n--;
		}

		else{
			int l=0;
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}
			current.getPrevious().setNext(current.getNext());
			current.getNext().setPrevious(current.getPrevious());
			n--;
		}
	}
	public void delete(T t) {
		if(head.getValue().equals(t)){
			deleteAtK(0);
			return;
		}
		else if(last.getValue().equals(t)){
			last=last.getPrevious();
			last.setNext(head);
			n--;
			return;
		}
		int k=0;
		current=head;
		while(current.hasNext()){
			k++;
			current=current.getNext();
			if(current.getValue().equals(t)){
				break;
			}
		}
		deleteAtK(k);
	}	

//	public T getElement(Integer k) throws NoSuchElementException{
//		T element=null;
//		if(isEmpty()){
//			throw new NoSuchElementException();
//		}
//		else if(k==0&&head!=null){
//			element= head.getValue();
//		}
//		else if(k==(n-1)){
//			element= last.getValue();
//		}
//		else{
//			current=head;
//			int l=0;
//			while(l<k){
//				current=current.getNext();
//				l++;
//			}
//			element= current.getValue();
//			current=head;			
//		}
//		return element;
//	} 	
	@SuppressWarnings("unchecked")
	public Iterator<T> iterator() throws NoSuchElementException{
		if(head==null){
			throw new NoSuchElementException();
		}	
		Object[] elements = new Object[n];
		current=head;
		int l =0;		
		while(l<n){
			elements[l]= current.getValue();
			current= current.getNext();
			l++;		
		}
		current=head;
		return (Iterator<T>) new model.data_structures.Iterator(elements);
	}
	public void add(T t) {
		addAtK(0, t);		
	}
	public T getElement(int k)throws NoSuchElementException {
		if(isEmpty()||k>=n){
			throw new NoSuchElementException();
		}		
		T elemento= null; int l=0;
		if(k==0){
			elemento= head.getValue();
		}
		else if(k==(n-1)){
			elemento=last.getValue();
		}
		else{
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}
			elemento=current.getValue();
		}
		return elemento;
	}
	
	public T getLast()
	{
		return last.getValue();
	}
}
