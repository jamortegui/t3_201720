package model.data_structures;
import java.util.NoSuchElementException;
public class Stack <T extends Comparable<T>> implements IStack<T>{
	private Node<T> head;
	private int size;

	/**
	 * M�todo constructor de clase.
	 */
	public Stack(){
		size = 0;
		head = null;
	}	
	/**
	 * Agrega un nuevo elemento a la pila
	 * @param pNuevo
	 * @throws NoSuchElementException
	 */
	public void push(T pNuevo) throws NoSuchElementException{
		if(pNuevo==null){
			throw new NoSuchElementException();
		}
		
		if(size > 0){
			Node<T> nuevo = new Node<T>(null, head, pNuevo);
			head = nuevo;
		}
		else
			head = new Node<T>(null, null, pNuevo);

		size++;		
	}
	/**
	 * Retorna el primer elemento y lo elimina de la pila.
	 * @return
	 */
	public T pop() throws NoSuchElementException{
		Node<T> aux = head;
		if(head==null)			throw new NoSuchElementException();
		if(size==1)
			head=null;
		
		else if(size>1)	
			head = head.getNext();
		
		size--;
		return aux.getValue();
	}
	
	public int size()
	{
		return size;
	}
}