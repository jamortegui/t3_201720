package model.vo;

import model.data_structures.DoubleLinkedList;
/**
 * Representation of a Stop object
 */
public class StopVO implements Comparable<StopVO>{
	private int stop_id;
	private String stop_code;
	private String stop_name;
	private String stop_desc;
	private double stop_lat;
	private double stop_lon;
	private String zone_Id;
	private String stop_url;
	private String location_type;
	private boolean transferProblems;
	private DoubleLinkedList<StopVO> paradas;
	private DoubleLinkedList<Integer> tiempos;
	public StopVO(int stop_id, String stop_code, String stop_name, String stop_desc, double stop_lat, double stop_lon,
			String zone_Id, String stop_url, String location_type) {
		super();
		this.stop_id = stop_id;
		this.stop_code = stop_code;
		this.stop_name = stop_name;
		this.stop_desc = stop_desc;
		this.stop_lat = stop_lat;
		this.stop_lon = stop_lon;
		this.zone_Id = zone_Id;
		this.stop_url = stop_url;
		this.location_type = location_type;
		this.transferProblems =  false;
	}
	public void istransferProblems(){
		transferProblems=true;
	}
	
	
	
	public int getStop_id() {
		return stop_id;
	}
	@Override
	public int compareTo(StopVO o) {
		// TODO Auto-generated method stub
		return 0;
	}
	public String getStop_code() {
		return stop_code;
	}
	public void setStop_code(String stop_code) {
		this.stop_code = stop_code;
	}
	public String getStop_name() {
		return stop_name;
	}
	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}
	public String getStop_desc() {
		return stop_desc;
	}
	public void setStop_desc(String stop_desc) {
		this.stop_desc = stop_desc;
	}
	public double getStop_lat() {
		return stop_lat;
	}
	public void setStop_lat(double stop_lat) {
		this.stop_lat = stop_lat;
	}
	public double getStop_lon() {
		return stop_lon;
	}
	public void setStop_lon(double stop_lon) {
		this.stop_lon = stop_lon;
	}
	public String getZone_Id() {
		return zone_Id;
	}
	public void setZone_Id(String zone_Id) {
		this.zone_Id = zone_Id;
	}
	public String getStop_url() {
		return stop_url;
	}
	public void setStop_url(String stop_url) {
		this.stop_url = stop_url;
	}
	public String getLocation_type() {
		return location_type;
	}
	public void setLocation_type(String location_type) {
		this.location_type = location_type;
	}
	public boolean isTransferProblems() {
		return transferProblems;
	}
	public void setTransferProblems(boolean transferProblems) {
		this.transferProblems = transferProblems;
	}
	public DoubleLinkedList<StopVO> getParadas() {
		return paradas;
	}
	public void setParadas(DoubleLinkedList<StopVO> paradas) {
		this.paradas = paradas;
	}
	public DoubleLinkedList<Integer> getTiempos() {
		return tiempos;
	}
	public void setTiempos(DoubleLinkedList<Integer> tiempos) {
		this.tiempos = tiempos;
	}
	public void setStop_id(int stop_id) {
		this.stop_id = stop_id;
	}	
	
}