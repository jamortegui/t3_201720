package model.vo;

import model.data_structures.DoubleLinkedList;

public class BusUpdateVO implements Comparable<BusUpdateVO>{
	
	private DoubleLinkedList<Double[]> listaDoble;
	
	
	private String nVehiculo;
	
	private Integer idTrip;
	private String nRuta;
	private String direccion;
	private String destino;
	private String patron;

	private String linkMapaRuta;
	
	public BusUpdateVO(String nVehiculo, Integer idTrip, String nRuta,
			String direccion, String destino, String patron, Double latitud,
			Double longitud, String tRegistrado, String linkMapaRuta) {
		super();
		listaDoble= new DoubleLinkedList<>();
		this.nVehiculo = nVehiculo;
		this.idTrip = idTrip;
		this.nRuta = nRuta;
		this.direccion = direccion;
		this.destino = destino;
		this.patron = patron;


		this.linkMapaRuta = linkMapaRuta;
	}
	
	
	public DoubleLinkedList<Double[]> getListaDoble() {
		return listaDoble;
	}


	public void agregarPosicion(Double[] a) {
		listaDoble.add(a);
	}


	@Override
	public int compareTo(BusUpdateVO arg0) {
		// TODO Auto-generated method stub
		return 0;
	} 
	public String getnVehiculo() {
		return nVehiculo;
	}


	@Override
	public String toString() {
		return "BusUpdateVO [ nVehiculo="
				+ nVehiculo + "]";
	}
	
}
